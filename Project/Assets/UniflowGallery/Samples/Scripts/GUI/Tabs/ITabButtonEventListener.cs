using UnityEngine;
using System.Collections;

public interface ITabButtonEventListener
{
	void OnTabButtonSelectedEvent( );
	void OnTabButtonDeselectedEvent( );
}
