using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor( typeof( UniflowGallery ) )]

/*
 * Custom UniflowGallery editor inspector
 *
 * v1.0
 * 	Initial Release
 * v1.1
 * 	Added fully working zoom, handlers.
 * 	Fixed weird zoom rotation effect (by computing shortest rotation)
 * 	Fixed Editor Inspector bug (gallery set as dirty at every frame)
 * v1.2
 *	Minor bug fix
 * 	Reuploading to Unity Asset Store...
 */

public class UniflowGalleryEditor : Editor {

	public override void OnInspectorGUI( )
	{
		UniflowGallery rUniflowGallery = target as UniflowGallery;

		DrawDefaultInspector( );

		EditorGUI.indentLevel = 1;

		// Gallery Layout
		EditorGUILayout.BeginHorizontal( );
		rUniflowGallery.galleryLayout = (UniflowGallery.EUniflowGalleryLayout) EditorGUILayout.EnumPopup( "Gallery Layout", rUniflowGallery.galleryLayout );
		EditorGUILayout.EndHorizontal( );

		// Selected thumbnail at start => range [0..thumbnail count - 1 ]
		EditorGUILayout.BeginHorizontal( );
		rUniflowGallery.selectedThumbnailAtStart = EditorGUILayout.IntField( "Selected Thumbnail At Start", rUniflowGallery.selectedThumbnailAtStart );
		EditorGUILayout.EndHorizontal( );

		// Thumbnail ratio => range ] 0..+infinity [
		EditorGUILayout.BeginHorizontal( );
		rUniflowGallery.thumbnailRatio = EditorGUILayout.FloatField( "Thumbnail Ratio", rUniflowGallery.thumbnailRatio );
		EditorGUILayout.EndHorizontal( );

		// Thumbnail spacing => range ] 0..+infinity ]
		EditorGUILayout.BeginHorizontal( );
		rUniflowGallery.thumbnailSpacing = EditorGUILayout.FloatField( "Thumbnail Spacing", rUniflowGallery.thumbnailSpacing );
		EditorGUILayout.EndHorizontal( );

		// Plane collider size => range ] 0..+infinity ]
		EditorGUILayout.BeginHorizontal( );
		rUniflowGallery.planeColliderSize = EditorGUILayout.FloatField( "Plane Collider Size", rUniflowGallery.planeColliderSize );
		EditorGUILayout.EndHorizontal( );

		if( GUI.changed == true )
		{
			Debug.Log( "changed!" );
			EditorUtility.SetDirty( rUniflowGallery );	
		}
	}
}
